#!/bin/sh

# Pass through the ENV of the docker containter
env >> /etc/environment

# Cats out the current Jobs
cat /var/spool/cron/crontabs/root

# execute CMD replacing the current PID 1
echo "$@"
exec "$@"